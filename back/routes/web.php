<?php

use Illuminate\Support\Facades\Route;

Route::post('/send', 'SendController@send');
Route::get('/cars', 'CarController@get');