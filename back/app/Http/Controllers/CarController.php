<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
/**
 * Class CarController
 * @package App\Http\Controllers
 */
class CarController extends CommonController {


    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function get() {
        return json_decode(Storage::disk('local')->get('cars/cars.json'),true);
    }

}
