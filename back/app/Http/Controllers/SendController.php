<?php

namespace App\Http\Controllers;

use Procontext\LPackage\Modules\FormHandler\Requests\SimpleAutoFormRequest;
use Procontext\LPackage\Modules\FormHandler\Services\FormAutoService;

/**
 * Class SendController
 * @package App\Http\Controllers
 */
class SendController extends CommonController {

    /**
     * @var FormAutoService
     */
    protected $service;

    /**
     * SendController constructor.
     * @param FormAutoService $service
     */
    public function __construct(FormAutoService $service) {
        $this->service = $service;
    }

    /**
     * @param SimpleAutoFormRequest $request
     * @return array
     */
    public function send(SimpleAutoFormRequest $request) {
        return $this->service->handle($request);
    }

}
