<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.11.18
 * Time: 12:46
 */

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;

class CommonController extends Controller {

    protected function wrapResponse($response, $wrapper = 'result') {
        return [$wrapper => $response];
    }

}