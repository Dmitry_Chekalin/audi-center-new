<?php

namespace App\Console\Commands;

use App\Utilities\CarsXmlParser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

/**
 * Class ParseCars
 * @package App\Console\Commands
 */
class ParseCars extends Command {

    protected $signature = 'parse:cars';

    protected $description = 'Parse cars';

    public function handle() {
        $cars = CarsXmlParser::parse('http://www.avtomir.ru/skoda_xml/ready_155.xml');
        if(array_has($cars, 'models') && !empty($cars['models'])) {
            Storage::disk('local')->put('cars/cars.json', json_encode($cars, JSON_UNESCAPED_UNICODE));
        }
    }
}
