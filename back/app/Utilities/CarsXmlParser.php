<?php

namespace App\Utilities;

use \XMLReader;

/**
 * Class CarsXmlParser
 * @package App\Utilities
 */
class CarsXmlParser {

    /** @var XMLReader $reader*/
    private $reader;
    private $path;
    private $data = [];

    private $currentModel;
    private $currentAuto;


    /**
     * CarsXmlParser constructor.
     * @param $path
     */
    public function __construct($path) {
        $this->path = $path;
        $this->reader = new XMLReader();
    }

    /**
     * @param string $path
     * @return array|mixed
     */
    public static function parse($path = '') {
        $parser = new self($path);
        return $parser->run();
    }

    /**
     * @return array|mixed
     */
    public function run() {
        if($this->reader->open($this->path)) {
            while ($this->reader->read()) {
                $this->parseNode($this->reader->localName);
            }
            $this->data = $this->formatteResData($this->data);
        }
        return $this->data;
    }

    /**
     * @param $node
     */
    private function parseNode(string $node) {
        switch ($node) {
            case 'brand':
                $this->data['brand'] = $this->reader->getAttribute('name');
                break;
            case 'model':
                $this->currentModel = $this->reader->getAttribute('name');
                break;
            case 'auto':
                if($this->reader->nodeType != XMLReader::END_ELEMENT) {
                    $this->currentAuto = $this->reader->getAttribute('id');
                    $this->data['models'][$this->currentModel][$this->currentAuto] = [
                        'package' => $this->reader->getAttribute('package'),
                        'body' => $this->reader->getAttribute('body'),
                        'transm' => $this->reader->getAttribute('transm'),
                        'patrol' => $this->reader->getAttribute('patrol'),
                        'volume' => $this->reader->getAttribute('volume'),
                        'hp' => $this->reader->getAttribute('hp'),
                        'year' => $this->reader->getAttribute('year'),
                        'price' => $this->reader->getAttribute('price'),
                        'interior' => $this->reader->getAttribute('interior'),
                        'interiorID' => $this->reader->getAttribute('interiorID'),
                        'status' => $this->reader->getAttribute('status'),
                    ];
                }
                break;
            case 'vin':
                $this->data['models'][$this->currentModel][$this->currentAuto]['vin'] = $this->reader->getAttribute('value');break;
            case 'price_dop':
                $this->data['models'][$this->currentModel][$this->currentAuto]['price_dop'] = $this->reader->getAttribute('value');break;
            case 'price_base':
                $this->data['models'][$this->currentModel][$this->currentAuto]['price_base'] = $this->reader->getAttribute('value');break;
            case 'color':
                $this->data['models'][$this->currentModel][$this->currentAuto]['color'] = [
                    'code' => $this->reader->getAttribute('code'),
                    'name' => $this->reader->getAttribute('name'),
                ];
                break;
            case 'equip':
                $this->data['models'][$this->currentModel][$this->currentAuto]['equip'][] = [
                    'ind' => $this->reader->getAttribute('ind'),
                    'value' => $this->reader->getAttribute('value'),
                ];
                break;
        }
    }


    /**
     * @param array $data
     * @return array
     */
    private function formatteResData(array $data): array {
        $res['brand'] = $data['brand'];
        foreach ($data['models'] as $model => $cars) {
            foreach ($cars as $id => $car) {
                $res['models'][] = [
                    'name' => $model,
                    'id' => $id,
                    'package' => $car['package'],
                    'body' => $car['body'],
                    'transm' => $car['transm'],
                    'patrol' => $car['patrol'],
                    'volume' => floatval(str_replace(',','.',$car['volume'])),
                    'hp' => (int)$car['hp'],
                    'year' => (int)$car['year'],
                    'price' => (int)$car['price'],
                    'interior' => $car['interior'],
                    'interiorID' => $car['interiorID'],
                    'status' => (int)$car['status'],
                    'vin' => $car['vin'],
                    'price_dop' => (int)$car['price_dop'],
                    'price_base' => (int)$car['price_base'],
                    'color' => $car['color']
                ];
            }
        }
        return $res;
    }
}