import CarsInfo from '../common/cars-info';
import CF from './common-functions';

let today = new Date();
let days_total = Math.floor((new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0)).getTime() / (1000 * 60 * 60 * 24));
let date = CF.getNewDate();
let next_date = new Date(2019,8,30,23);
let trigger = new Date(2019,8,30,23);


const PHONE_1 = '+7 (495) 755-82-82'.replace(/\s/g, '\u00A0').replace(/-/g, '\u2011');
const PHONE_2 = '+7 (495) 755-88-11'.replace(/\s/g, '\u00A0').replace(/-/g, '\u2011');
const PHONE_3 = '+7 (495) 755-81-81'.replace(/\s/g, '\u00A0').replace(/-/g, '\u2011');

const MAX_ADVANTAGE = (function () {
    let info = CarsInfo.CARS;
    let max = info[0].advantage;
    for (let i = 0; i < info.length; i++) {
        if (info[i].advantage > max) {
            max = info[i].advantage
        }
    }
    return max;
})();
const MIN_CREDIT = (function () {
    let info = CarsInfo.CARS;
    let min = info[0].credit_payment;
    for (let i = 0; i < info.length; i++) {
        if (info[i].credit_payment < min) {
            min = info[i].credit_payment
        }
    }
    return min;
})();

const PHONE_RAW_1 = PHONE_1.replace(/\D+/g, "");
const PHONE_RAW_2 = PHONE_2.replace(/\D+/g, "");
const PHONE_RAW_3 = PHONE_3.replace(/\D+/g, "");


const SALONS = [
        {
            name: 'Центр Восток',
            address: 'г. Москва, ш. Энтузиастов, д.12Б (3 км от МКАД)',
            phone: PHONE_1,
            worktime: 'Ежедневно с 9:00 до 22:00',
            phone_raw: PHONE_RAW_1,
            coords: {
                x_center_desktop: 37.887525,
                x_center_tablet: 37.887525,
                x_center_mobile: 37.887525,
                y_center_desktop: 55.788636,
                y_center_tablet: 55.788636,
                y_center_mobile: 55.788636,
                x_salon: 37.887525,
                y_salon: 55.788636,
            },
        },
        {
            name: 'Центр Варшавка',
            address: 'г. Москва, Варшавское шоссе, 91А',
            phone: PHONE_2,
            worktime: 'Ежедневно с 9:00 до 22:00',
            phone_raw: PHONE_RAW_2,
            coords: {
                x_center_desktop: 37.620941,
                x_center_tablet: 37.620941,
                x_center_mobile: 37.620941,
                y_center_desktop: 55.650277,
                y_center_tablet: 55.650277,
                y_center_mobile: 55.650277,
                x_salon: 37.620941,
                y_salon: 55.650277,
            },
        },
        {
            name: 'Центр Таганка',
            address: 'г. Москва, Михайловский пр-д, д. 3, стр. 25',
            phone: PHONE_3,
            worktime: 'Ежедневно с 8:00 до 22:00',
            phone_raw: PHONE_RAW_3,
            coords: {
                x_center_desktop: 37.684892,
                x_center_tablet: 37.684892,
                x_center_mobile: 37.684892,
                y_center_desktop: 55.730933,
                y_center_tablet: 55.730933,
                y_center_mobile: 55.730933,
                x_salon: 37.684892,
                y_salon: 55.730933,
            },
        }
    ];

const MIXED_SALONS = SALONS.sort(() => 0.5 - Math.random());

export default {
    salons: MIXED_SALONS,
    max_advantage: MAX_ADVANTAGE,
    min_credit: MIN_CREDIT,
    end_date:  today - trigger > 0 ? date : trigger,
    need_agreement: true,
    show_car_params: true,
    cKeeper: 'actioncall',
}
