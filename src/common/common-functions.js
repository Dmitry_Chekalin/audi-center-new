const MONTH_NAMES = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря'
];

export default {
    getNumericalName(number, variants) {
        const numerals = {
            days: ['день', 'дня', 'дней'],
            hours: ['час', 'часа', 'часов'],
            minutes: ['минута', 'минуты', 'минут'],
            seconds: ['секунда', 'секунды', 'секунд'],
            rubles: ['рубль', 'рубля', 'рублей'],
            bug: ['баг', 'бага', 'багов']
        };
        if (!Array.isArray(variants)) {
            variants = numerals[variants] || numerals.bug;
        }
        let temp = number;
        while (number >= 10) {
            number = number % 10;
        }
        while (temp >= 100) {
            temp = temp % 10;
        }
        if (number > 0 && number < 5) {
            if (temp > 10 && temp < 15) {
                return variants[2];
            }
            if (number == 1) return variants[0];
            return variants[1];
        }
        return variants[2];
    },
    getMonthEnd() {
        let today = new Date();
        return new Date(today.getFullYear(),today.getMonth() + 1, 0).getDate() +
            ' ' + MONTH_NAMES[today.getMonth()] + ' ' + today.getFullYear() + 'г';
    },
    getNewDate() {
        const WEEK = 604800000;
        const DAY = 86400000;
        let today = new Date();
        let next = new Date(2019, 8, 30, 22, 0, 0);
        let time1 = new Date(2019, 8, 30, 22, 0, 0); //Дата запланированного изменения
        let delta = 5*DAY;
        if (today - time1 >= 0) {
            next.setDate(30);
            next.setMonth(8);
        };
        while (today.valueOf() - next.valueOf() > 0) {
            console.log('tick');
            next = new Date(next.valueOf() + delta);
        }

        return next;
    },
    getDateString() {
        return this.getNewDate().getDate() + ' ' + this.matchMonth(this.getNewDate().getMonth());
    },
    matchMonth(month) {
        return MONTH_NAMES[month];
    }
}
