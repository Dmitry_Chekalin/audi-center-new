const PRICES = {
	q5: 3200000,
	q7: 3995000,
	q8: 5060000
};

export default {
    CARS: [
        {
            model: 'q5',
            name: 'Audi Q5',
			price: PRICES.q5,
			bonuses: [
				'в кредит от 5%<sup>7</sup>',
				'полис каско в подарок<sup>8</sup>',
				'ТО-0 и ТО-1 в подарок<sup>5</sup>',
				'бонус по программе трейд-ин <span style="white-space: nowrap;">150 000 ₽</span><sup>2</sup>',
			]
            
        },
        {
            model: 'q7',
            name: 'Audi Q7',
			price: PRICES.q7,
			bonuses: [
				'в кредит от 9,9%<sup>7</sup>',
				'ТО-0 и ТО-1 в подарок<sup>5</sup>',
				'бонус по программе трейд-ин <span style="white-space: nowrap;">200 000 ₽</span><sup>2</sup>',
				'3.0 TDI от 4 048 230 ₽',
				'полис каско в подарок<sup>8</sup>'
			]
           

        },
        {
            model: 'q8',
            name: 'Audi Q8',
			price: PRICES.q8,
			bonuses: [
				'в кредит от 9,9%<sup>7</sup>',
				'полис каско в подарок<sup>8</sup>',
				'бонус по программе трейд-ин <span style="white-space: nowrap;">200 000 ₽</span><sup>2</sup>',
				'Программа лояльности <span style="white-space: nowrap;">«Лига привилегий Audi»</span><sup>10</sup>'
			]
          
        }
    ]

};
