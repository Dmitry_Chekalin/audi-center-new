/*swiperConfig:   -- should be placed in component data
    ref: string with name of swiper attribute ref
    params: object of swiper`s params (second argument of swiper constructor)
    enable_hashing: boolean, allows adding hash of entity to current url
    entities: swiper slide objects (for example, entities of v-for) - not required, if 'enable_hashing' is false
    forbidden_platforms: array of strings with names of platforms where swiper shouldn`t appear
 */

import Swiper from 'swiper';

export default {
    data() {
        return {
            swipersConfigs: [],
            swipers: []
        }
    },
    watch: {
        device_platform(new_platform) {
            this.$nextTick(() => {
                if (this.swipers && this.swipers.length === 0) {
                    this.swipersInit();
                } else if (this.swipers.some((swiper) => swiper.forbidden_platforms.indexOf(new_platform) > -1)) {
                    this.swipers.forEach(swiper => swiper.destroy());
                    this.swipers = [];
                    this.swipersInit();
                }
            });
        }
    },
    mounted() {
        history.scrollRestoration = 'manual';
        this.swipersInit();
        if (window.location.hash && window.location.hash.length) {
            let hash = window.location.hash.replace('#', '');
            let item_index;
            let swiper_index = this.swipers.findIndex(swiper => {
                if (!swiper.entities) {
                    return false;
                }
                item_index = swiper.entities.findIndex(item => item.hash_name === hash);
                return item_index > -1;
            });
            if (hash.length && swiper_index > -1) {
                this.$nextTick(() => {
                    this.swipers[swiper_index].slideTo(item_index);
                    this.$emit('scrollTo', this.$el.id);
                });
            }
            else {
                this.$emit('scrollTo', hash);
            }
        }
    },
    methods: {
        swipersInit() {
            this.swipersConfigs.forEach(config => {
                if (config.forbidden_platforms.indexOf(this.device_platform) > -1) {
                    return
                }
                let swiper = new Swiper(this.$refs[config.ref], config.params);
                if (config.enable_hashing) {
                    swiper.on('slideChangeTransitionEnd', function () {
                        let new_hash = '#' + config.entities[swiper.activeIndex].hash_name;
                        history.replaceState(null, null, new_hash);
                    });
                }
                swiper.entities = config.entities;
                swiper.forbidden_platforms = config.forbidden_platforms;
                this.swipers.push(swiper);
                console.log(config)
            });

        }
    }
}

