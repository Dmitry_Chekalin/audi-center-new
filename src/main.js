import Vue from 'vue';
import App from './App.vue';
import axios from 'axios';
import Constants from './common/constants';
import "./styles/main.scss";

const EventBus = new Vue();

// Tell Vue to use the plugin
Vue.prototype.axios = axios;
Vue.prototype.CONSTANTS = Constants;
Vue.prototype.$eventBus = EventBus;
Vue.config.productionTip = false;

new Vue({
    el: '#app',
    template: '<App/>',
    components: {App}
});
