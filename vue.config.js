const path = require('path');
//const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
    chainWebpack: config => {
        config.module.rule('images')
            .uses
            .clear();

        config.module
            .rule('responsive-loader')
            .test(/\.(jpe?g|png)/i)
            .use('responsive-loader')
            .loader('responsive-loader')
            .tap(() => {
                    return {
                        adapter: require('responsive-loader/sharp'),
                        name: 'img/[name]-[width].[hash].[ext]',
                        quality: 95
                    }
                }
            );
        config.plugin('preload-font')
            .use(require('@vue/preload-webpack-plugin'), [{
                rel: 'preload',
                include: 'allAssets',
                fileWhitelist: [/\.woff$/],
                as: 'font'
            }]);
    },
    configureWebpack: {
        plugins: [
            /*new FaviconsWebpackPlugin({
                logo: './src/images/favicon.png',
                prefix: 'icons/',
                emitStats: false,
                statsFilename: 'iconstats.json',
                inject: true,
                persistentCache: false
            }),*/
        ],
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
                vue$: 'vue/dist/vue.esm.js',
                '@': path.join(__dirname, 'src'),
                'styles': path.join(__dirname, 'src/styles'),
                'swiper$': 'swiper/dist/js/swiper.js'
            }
        }
    },
    outputDir: 'build',
};
